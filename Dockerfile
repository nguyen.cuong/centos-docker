FROM centos:7

MAINTAINER Nguyen Cuong <nguyen.cuong@rivercrane.vn>

ADD  etc/yum.repos.d/* /etc/yum.repos.d/

RUN   yum update -y
RUN   yum install -y nginx

## Install require to use
RUN   yum install -y wget patch tar bzip2 unzip openssh-clients gcc pcre-devel zlib-devel make git

# Yum release action
RUN   yum remove  epel-release
RUN   yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
RUN   yum install epel-release -y
RUN   yum update -y

RUN   yum --enablerepo=remi-php73 install -y php php-fpm php-bcmath php-cli php-common php-devel php-gd php-gmp php-intl php-json php-mbstring php-mcrypt php-mysqlnd php-opcache php-pdo php-pear php-process php-pspell php-xml php-pecl-imagick php-pecl-mysql php-pecl-uploadprogress php-pecl-uuid php-pecl-zip php-pecl-ssh2 php-redis

RUN   php --version

  # Disable SSH strict host key checking: needed to access git via SSH in non-interactive mode
RUN      echo -e "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
# Install composer
RUN   curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
      chown nginx:nginx /usr/local/bin/composer && \
      composer --version
RUN yum install -y supervisor

## Install require
RUN   yum install -y ImageMagick lftp

COPY ./etc /etc/

EXPOSE  80 443 9000 9001

RUN mkdir -p /data/logs/

RUN mkdir -p /run/php-fpm/ && touch -f /run/php-fpm/php-fpm.pid && touch nginx.pid

CMD [ "/bin/bash","/etc/bootservice.sh" ]

# Clean YUM caches to minimise Docker image size... #
RUN yum clean all && rm -rf /tmp/yum*
